import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Partita {

	public static void main(String[] args) {

		Game g = new Game();

		Logger log = LoggerFactory.getLogger(Partita.class);
		
		
		g.roll(5);
		g.roll(3);
		
		log.info("Punteggio : {}",g.score());
		
		g.roll(4);
		g.roll(4);
		
		log.info("Punteggio : {}",g.score());
	}

}
