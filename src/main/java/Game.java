
public class Game {
	
	private int score = 0;
	private int tentativo = 1;
	private boolean spare = false;
	private boolean strike = false;
	private int birilli = 10;
	
	private int[] punteggio = new int[100];
	private int frame = 0;
	
	public void roll(int pins) {
		if(tentativo==1 && pins!=10){
			if(spare == true){
				score += pins*2;
				spare = false;
			}
			else{
				score += pins;
			}
			birilli -= pins;
			tentativo = 2;
			
		}
		else if(tentativo==1 && pins==10){
			if(strike==false){
				punteggio[frame]=10;
			}
			else{
				punteggio[frame]=20;
			}
			frame++;
			strike = true;
			
		}
		
		else{
			
			if(tentativo==2){
				birilli -= pins;
				tentativo = 1;
				if(strike==true){
					punteggio[frame] = (score+pins)*2;
					frame++;
					birilli = 10;
					score=0;
				}
				else{
					if(birilli==0){
						spare = true;
						birilli = 10;
					}
					punteggio[frame] = score+pins;
					frame++;
					birilli = 10;
					score=0;
				}
			}

		}
		
		
		
	}

	public int score() {
		int punti = 0;
		for(int p : punteggio){
			punti += p;
		}
		return punti;
	}

}
